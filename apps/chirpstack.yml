# https://www.chirpstack.io/
# https://github.com/chirpstack/chirpstack-docker
services:
  # https://hub.docker.com/r/chirpstack/chirpstack/tags
  chirpstack:
    image: chirpstack/chirpstack:4
    command: -c /etc/chirpstack
    restart: unless-stopped
    volumes:
      - /home/debian/chirpstack-docker/configuration/chirpstack:/etc/chirpstack
    #  - /home/debian/chirpstack-docker/lorawan-devices:/opt/lorawan-devices
    depends_on:
      - postgres
      - mosquitto
      - redis
    environment:
      - MQTT_BROKER_HOST=mosquitto
      - REDIS_HOST=redis
      - POSTGRESQL_HOST=postgres
    labels:
      - traefik.enable=true
      - traefik.docker.network=frontend
      - traefik.http.routers.chirpstackui-secure.entrypoints=websecure
      - traefik.http.routers.chirpstackui-secure.rule=Host(`app-server.mondomaine.org`)
    networks:
      - frontend
      - backend
    ports:
      - 8080:8080

  # https://hub.docker.com/r/chirpstack/chirpstack-gateway-bridge/tags
  # UDP
  # chirpstack-gateway-bridge:
  #   image: chirpstack/chirpstack-gateway-bridge:4
  #   restart: unless-stopped
  #   ports:
  #     - 1700:1700/udp
  #   volumes:
  #     - /home/debian/chirpstack-docker/configuration/chirpstack-gateway-bridge:/etc/chirpstack-gateway-bridge
  #   environment:
  #     - INTEGRATION__MQTT__EVENT_TOPIC_TEMPLATE=eu868/gateway/{{ .GatewayID }}/event/{{ .EventType }}
  #     - INTEGRATION__MQTT__STATE_TOPIC_TEMPLATE=eu868/gateway/{{ .GatewayID }}/state/{{ .StateType }}
  #     - INTEGRATION__MQTT__COMMAND_TOPIC_TEMPLATE=eu868/gateway/{{ .GatewayID }}/command/#
  #   depends_on:
  #     - mosquitto

  # https://hub.docker.com/r/chirpstack/chirpstack-gateway-bridge/tags
  # Basic station   
  chirpstack-gateway-bridge-basicstation:
    image: chirpstack/chirpstack-gateway-bridge:4
    restart: unless-stopped
    command: -c /etc/chirpstack-gateway-bridge/chirpstack-gateway-bridge-basicstation-eu868.toml
    labels:
      - traefik.enable=true
      - traefik.docker.network=frontend
      - traefik.http.routers.chirpstackws-secure.entrypoints=websecure  # Pour WSS (WebSocket Secure)
      - traefik.http.routers.chirpstackws-secure.rule=Host(`ws.mondomaine.org`)  # Sous-domaine pour WebSocket
      - traefik.http.services.chirpstackws-secure.loadbalancer.server.port=3001  # Redirection vers le port interne 3001
      - traefik.http.routers.chirpstackws-secure.service=chirpstackws-secure
      - traefik.http.middlewares.websocket.headers.customrequestheaders.Upgrade=websocket  # En-tête pour WebSocket
      - traefik.http.middlewares.websocket.headers.customrequestheaders.Connection=Upgrade  # En-tête pour WebSocket
    networks:
      - frontend
      - backend
    ports:
      - 3001:3001
    volumes:
      - /home/debian/chirpstack-docker/configuration/chirpstack-gateway-bridge:/etc/chirpstack-gateway-bridge
    depends_on:
      - mosquitto

  # https://hub.docker.com/r/chirpstack/chirpstack-rest-api/tags
  chirpstack-rest-api:
    image: chirpstack/chirpstack-rest-api:4
    restart: unless-stopped
    command: --server chirpstack:8080 --bind 0.0.0.0:8090 --insecure
    labels:
      - traefik.enable=true
      - traefik.docker.network=frontend
      - traefik.http.routers.chirpstackapi-secure.entrypoints=websecure
      - traefik.http.routers.chirpstackapi-secure.rule=Host(`api.mondomaine.org`)
    networks:
      - frontend
      - backend
    ports:
      - 8090:8090
    depends_on:
      - chirpstack

  # https://hub.docker.com/_/postgres/tags
  postgres:
    image: postgres:14-alpine
    restart: unless-stopped
    volumes:
      - /home/debian/chirpstack-docker/configuration/postgresql/initdb:/docker-entrypoint-initdb.d
      - postgresqldata:/var/lib/postgresql/data
    environment:
      - POSTGRES_PASSWORD=root
    networks:
      - backend

  # https://hub.docker.com/_/redis/tags
  redis:
    image: redis:7-alpine
    restart: unless-stopped
    command: redis-server --save 300 1 --save 60 100 --appendonly no
    volumes:
      - redisdata:/data
    networks:
      - backend

  # https://hub.docker.com/r/eclipse/mosquitto/tags
  mosquitto:
    image: eclipse-mosquitto:2
    restart: unless-stopped
    ports:
      - 1883:1883
    volumes: 
      - /home/debian/chirpstack-docker/configuration/mosquitto/config/:/mosquitto/config/
    networks:
      - backend

volumes:
  postgresqldata:
  redisdata:

networks:
  frontend:
    external: true
  backend:
    external: true