#!/usr/bin/env bash
#
# Description : Installation de Traefik et Portainer
# Usage : ./install-traefik-portainer.sh
# Licence : GPL-3+
# Auteur : Cédric Goby
# Versioning : https://forgemia.inra.fr/cedric.goby/traefik-portainer-compose
#
#-----------------------------------------------------------------------
# Déploiement
#-----------------------------------------------------------------------
# Création des réseaux docker
docker network create frontend && docker network create backend
# Prompt nom de domaine
read -p "Nom de domaine : " _domain_name
# Prompt email Let's encrypt
read -p "Email pour Let's encrypt : " _letsencrypt_email
# Prompt mot de passe pour le compte "admin" de Traefik
begin=true
again=false
while $begin; do
	$again && echo "Les mots de passe ne sont pas identiques. Veuillez réessayer." || again=true
	begin=false	
	read -rs -p "Mot de passe admin pour Traefik : " _traefik_admin_passwd
	echo
	read -rs -p "Confirmation du mot de passe : " _password_confirm
	echo		
	if [[ "$_traefik_admin_passwd" != "$_password_confirm" ]]; then
	    begin=true
	fi
done
# Renseigner les identifiants pour traefik dans `core/traefik-data/configurations/dynamic.yml`
_traefik_encrypted_passwd="$(htpasswd -nb admin "$_traefik_admin_passwd")"
# Insertion d'antislash devant les caractères ayant une signification pour sed
_traefik_encrypted_passwd="$(<<< "$_traefik_encrypted_passwd" sed -e 's`[][\\/.*^$]`\\&`g')"
sed -i "s/_traefik_encrypted_passwd/$_traefik_encrypted_passwd/g" core/traefik-data/configurations/dynamic.yml
# Renseigner l'adresse email pour Let's Encrypt dans `core/traefik-data/traefik.yml`.
sed -i "s/_letsencrypt_email/$_letsencrypt_email/g" core/traefik-data/traefik.yml
# Renseigner les noms de domaine pour traefik et portainer dans `core/docker-compose.yml`.
sed -i "s/_domain_name/$_domain_name/g" core/docker-compose.yml
# Droits sur le fichier acme.json
chmod 600 core/traefik-data/acme.json
# Lancement des containers traefik et portainer
docker compose -f core/docker-compose.yml up -d
